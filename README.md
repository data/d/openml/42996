# OpenML dataset: gender-by-name

https://www.openml.org/d/42996

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Arun Rao
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Gender+by+Name) - 2020
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Gender by Name Data Set**

This dataset attributes first names to genders, giving counts and probabilities. It combines open-source government data from the US, UK, Canada, and Australia.

This dataset combines raw counts for first/given names of male and female babies in those time periods, and then calculates a probability for a name given the aggregate count. Source datasets are from government authorities: 

- US: Baby Names from Social Security Card Applications - National Data, 1880 to 2019 
- UK: Baby names in England and Wales Statistical bulletins, 2011 to 2018 
- Canada: British Columbia 100 Years of Popular Baby names, 1918 to 2018 
- Australia: Popular Baby Names, Attorney-General's Department, 1944 to 2019

### Attribute information
- Name: String 
- Gender: M/F (category/string) 
- Count: Integer 
- Probability: Float

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42996) of an [OpenML dataset](https://www.openml.org/d/42996). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42996/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42996/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42996/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

